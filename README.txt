=====================================================
Dockerized FIWARE PEP Proxy implementation - v1.0
=====================================================


.:: CONTENTS ::.

This readme allows the installation of the FIWARE PEP Proxy released through Docker.


.:: SETUP ::.

BUILD AND RUN DOCKER IMAGES

Execute in the folder 'docker-pep-wilma' the command for setting up Docker compose:
	docker-compose up


.:: CHANGELOG ::.

v1.0 Tested on the development machine


.:: CONTACTS ::.

alessio.roppolo@eng.it
